# This is file that uses the trend line analysis in the form of rolling window

from TradingData import *
from newPlotter import *

import numpy as np
import pandas as pd
from scipy.signal import argrelextrema

from copy import deepcopy
import math
from heapq import heapify, heappush, heappop

import time

# Class for a trendline
class TLine:
    
    def __init__(self, _i1, _i2, _ptx, _pty, _slp, _intercpt, _trend, _score):
        self.i1 = _i1                           # left end point
        self.i2 = _i2                           # right end point 
        self.ptx = _ptx                         # x coordinate of extremas by joining whom this line is made
        self.pty = _pty                         # y coordinate of the extrema
        self.slp = _slp                         # slope of trendline
        self.intercpt = _intercpt               # intercpt of trendline
        self.trend = _trend                     # whether the line is up or down
        self.score = _score                     # score of line sees if the line crossed the threshold to put in reserve set
        
    def __eq__(self, other):                    # equality function
        
        if self.ptx[0] == other.ptx[0] and self.ptx[1] == other.ptx[1] and self.slp == other.slp:
            return True
        return False


class TrendLineAnalysis:
    
    def __init__(self, df, n:int = 20):
        
        self.df = df                                # dataset
        self.n = n                                  # minimum lenght of channel
        
        self.df['minval'] = self.df[['open', 'close']].min(axis=1)     # obtaining the maximum and minimum of open and close
        self.df['maxval'] = self.df[['open', 'close']].max(axis=1)
        
        self.df['min'] = self.df.iloc[argrelextrema(self.df.low.values, np.less_equal, order = (self.n//3))[0]]['low']
        self.df['max'] = self.df.iloc[argrelextrema(self.df.high.values, np.greater_equal, order = (self.n//3))[0]]['high']
        
        self.new_count = 0                                  # keeps the count of new candlestick added 
        self.price_range = self.df['max'].max() - self.df['min'].min()      # diff of max and min
    
        self.window_size = self.n * 10                      # window size for the rolling window
        
        # set of lines which are made and could get extended
        self.extendresistance = self.FindResistance(0, self.window_size)        
        self.extendsupport = self.FindSupport(0, self.window_size)
        
        # set of reserve line which won't be used further
        self.resistance = []                
        self.support = []
        
        # temporary df where data is added
        self.temp_df = pd.DataFrame(columns=self.df.columns)
        
        # see function description
        self.CompleteExtension()
        
    # Finds the set of resistance in the data between the start and end
    def FindResistance(self, start, end):
        # Changing the find function to check to all values
        dfMax = self.df[self.df['max'].notnull()]
        resistance = []
        wind = self.n//3
        
        for i1, p1 in dfMax.iterrows():
            
            if p1['candleID'] < start:
                continue
            elif p1['candleID'] > end:
                break 
            
            for i2, p2 in dfMax.iterrows():
                
                if p2['candleID'] < start:
                    continue
                elif p2['candleID'] > end:
                    break
                
                if i1 + 1 < i2 and (p1['candleID'] < p2['candleID'] - wind):
                
                    pty = np.asarray((p1['max'], p2['max']))
                    ptx = np.asarray((p1['candleID'], p2['candleID']))
                    
                    slmx, intermx = np.polyfit(ptx, pty, 1)
                    valid_idx = []
                    
                    # for i3 in range(i1+1, i2-1):
                    #     if not pd.isna(self.df.iloc[i3,:]['max']):
                    #         p3 = self.df.iloc[i3,:]
                    #         tempx = p3['candleID']
                    #         tempy = slmx*tempx + intermx
                            
                    #         if tempy < p3['maxval']: # trend is broken
                    #         # if tempy < p3['max']:
                    #             valid_idx = []
                    #             break        
                    #         v_temp = tempy - p3['max']
                    #         if v_temp < self.price_range*0.1:
                    #             valid_idx.append(i3)   
                            
                    for i3 in range(p1['candleID']+1,  p2['candleID']):
                        p3 = self.df.iloc[i3,:]
                        tempx = p3['candleID']
                        tempy = slmx*tempx + intermx
                        
                        if tempy < p3['maxval']:
                            valid_idx = []
                            break 
                        
                        if not pd.isna(self.df.iloc[i3,:]['max']):
                            v_temp = abs(tempy - p3['max'])
                            if v_temp < self.price_range*0.1:
                                valid_idx.append(i3)   
                    
                    if (p1['max'] <= p2['max']):   #Possible Uptrend
                        if len(valid_idx) > 0:
                            # print("yes1")
                            tline = TLine(i1, i2, ptx, pty, slmx, intermx, "up", 1)
                            resistance.append(tline)
                            
                    else:                           # possible down trend
                        if len(valid_idx) > 0:
                            # print("yes2")
                            tline = TLine(i1, i2, ptx, pty, slmx, intermx, "down", 1)
                            resistance.append(tline)
        
        return resistance                   
    
    def FindSupport(self, start, end):
        
        dfMin = self.df[self.df['min'].notnull()]
        wind = self.n//3
        support = []
        for i1, p1 in dfMin.iterrows():
            
            if p1['candleID'] < start:
                continue
            elif p1['candleID'] > end:
                break 
            
            for i2, p2 in dfMin.iterrows():
                
                if p2['candleID'] < start:
                    continue
                elif p2['candleID'] > end:
                    break
                
                if i1 + 1 < i2 and (p1['candleID'] < p2['candleID'] - wind):
                
                    pty = np.asarray((p1['min'], p2['min']))
                    ptx = np.asarray((p1['candleID'], p2['candleID']))
                    
                    slmx, intermx = np.polyfit(ptx, pty, 1)
                    valid_idx = []
                    
                    # for i3 in range(i1+1, i2-1):
                    #     if not pd.isna(self.df.iloc[i3,:]['min']):
                    #         p3 = self.df.iloc[i3,:]
                    #         tempx = p3['candleID']
                    #         tempy = slmx*tempx + intermx
                            
                    #         if tempy > p3['minval']: # trend is broken
                    #         # if tempy > p3['min']: 
                    #             valid_idx = []
                    #             break        
                                
                    #         v_temp = tempy - p3['min']
                    #         if v_temp < self.price_range*0.1:
                    #             valid_idx.append(i3)
                    
                    for i3 in range(p1['candleID']+1,  p2['candleID']):
                        p3 = self.df.iloc[i3,:]
                        tempx = p3['candleID']
                        tempy = slmx*tempx + intermx
                        
                        if tempy > p3['minval']:
                            valid_idx = []
                            break 
                        
                        if not pd.isna(self.df.iloc[i3,:]['min']):
                            v_temp = abs(tempy - p3['min'])
                            if v_temp < self.price_range*0.1:
                                valid_idx.append(i3)
                    
                    
                    if (p1['min'] <= p2['min']):   #Possible Uptrend
                        if len(valid_idx) > 0:
                            # print("yes3")
                            tline = TLine(i1, i2, ptx, pty, slmx, intermx, "up", 1)
                            support.append(tline)
                            
                    else:                           # possible down trend
                        if len(valid_idx) > 0:
                            # print("yes4")
                            tline = TLine(i1, i2, ptx, pty, slmx, intermx, "down", 1)
                            support.append(tline) 
        
        return support   
    
    # Filter the set of lines having same endpoints and very similar slope near to equal
    def FilterSameLines(self, lines):
        
        remove_lines = []
        wind = self.n//3
        
        for t1 in lines:
            if t1 in remove_lines:
                continue
            
            for t2 in lines:
                if t2 in remove_lines:
                    continue
                
                if t1 == t2:
                    continue
                
                l1 = math.sqrt((t1.ptx[0] - t1.ptx[1])**2) + ((t1.pty[0] - t1.pty[1])**2) 
                l2 = math.sqrt((t2.ptx[0] - t2.ptx[1])**2) + ((t2.pty[0] - t2.pty[1])**2)

                slp1 = math.atan(t1.slp)
                slp2 = math.atan(t2.slp)
                x_diff = max( abs(t1.ptx[0] - t1.ptx[1]), abs(t2.ptx[0] - t2.ptx[1]) )
                
                if x_diff*(abs(t1.slp - t2.slp)) < self.price_range*0.1:
                    if (( abs(t1.ptx[0] - t2.ptx[0]) <= wind*2) or ( abs(t1.ptx[1] - t2.ptx[1]) <= wind*2)):
                        if l1 < l2:
                            remove_lines.append(t1)
                        else:
                            remove_lines.append(t2)
                            
        return remove_lines                        
    
    # removes the filtered resistance line
    def FilterResistanceLines(self):
        
        remove_resistance = self.FilterSameLines(self.resistance)
        for t in remove_resistance:
            if t in self.resistance:
                self.resistance.remove(t)
                
        remove_resistance = self.FilterSameLines(self.extendresistance)
        for t in remove_resistance:
            if t in self.extendresistance:
                self.extendresistance.remove(t)
        
    # removes the filtered support line
    def FilterSupportLines(self):
        
        remove_support = self.FilterSameLines(self.support)
        for t in remove_support:
            if t in self.support:
                self.support.remove(t)
                
        remove_support = self.FilterSameLines(self.extendsupport)
        for t in remove_support:
            if t in self.extendsupport:
                self.extendsupport.remove(t)
                
    # gives a value to line whether it satisfies the point or not         
    def ScoreLine(self, line, xval, yval, flag):
        
        slp = line.slp
        intrmx = line.intercpt
        pred = (slp * xval)  + intrmx
        
        if flag == 0:   # for support 
            if pred > yval:
                return -1
            else:
                if abs(yval - pred) < self.price_range*0.2:
                    return 1
                else:
                    return 0
        else:             # for resistance 
            if pred < yval:
                return -1
            else:
                if abs(yval - pred) < self.price_range*0.2:
                    return 1
                else:
                    return 0
    
    # function to update the dataset with new candlestick when reached a limit
    def UpdateExtremas(self):
        
        # self.temp_df = pd.concat([self.df[:-self.n], self.temp_df])
        
        self.temp_df['min'] = self.temp_df.iloc[argrelextrema(
            self.temp_df.low.values, np.less_equal, order = (self.n//3))[0]]['low']
        self.temp_df['max'] = self.temp_df.iloc[argrelextrema(
            self.temp_df.high.values, np.greater_equal, order = (self.n//3))[0]]['high']

        self.df = pd.concat([self.df, self.temp_df])
        self.price_range = self.df['max'].max() - self.df['min'].min()
        self.temp_df = pd.DataFrame(columns=self.df.columns)

    # function to update the set of lines and add new lines which could have been formed due to new extremas found
    def UpdateLines(self):
        
        tmp_support = self.FindSupport(self.new_count, self.new_count + self.window_size)
        tmp_resistance = self.FindResistance(self.new_count, self.new_count + self.window_size)

        rm = []
        for t1 in tmp_support:
            if t1 in self.support:
                rm.append(t1)
            if t1 in self.extendsupport:
                rm.append(t1)
        for t in rm:
            if t in tmp_support:
                tmp_support.remove(t) 
        
        for t in tmp_support:
            self.extendsupport.append(t)

        rm = []
        for t1 in tmp_resistance:
            if t1 in self.resistance:
                rm.append(t1)
            if t1 in self.extendresistance:
                rm.append(t1)
        for t in rm:
            if t in tmp_resistance:
                tmp_resistance.remove(t) 
        
        for t in tmp_resistance:
            self.extendresistance.append(t)
            
        self.CompleteExtension()
    
    # checks if the line is extensible till the end x coordinate or not
    # flag gives the value if the line is resistance or support 
    def CheckExtendLine(self, line, end, flag):
        
        for i in range(line.i2+1, end+1):
            p = self.df.iloc[i,:]
            xval = p['candleID']
            slp = line.slp
            intrmx = line.intercpt
            pred = (xval*slp) + intrmx
            
            if flag == 1:   # for resistance
                if pred < p['maxval']:
                    line.i2 = xval
                    return -1
                else:
                    if abs(p['maxval'] - pred) < self.price_range*0.2:
                        if line.score < 10:
                            line.score = 1
                        continue
                    else:
                        line.score = line.score+1
                        if line.score >= 10:
                            line.i2 = xval
                            return -1
                        
            else:          # for support
                if pred > p['minval']:
                    line.i2 = xval
                    return -1
                else:
                    if abs(p['minval'] - pred) < self.price_range*0.2:
                        if line.score < 10:
                            line.score = 1
                        continue
                    else:
                        line.score = line.score+1
                        if line.score >= 10:
                            line.i2 = xval
                            return -1
        line.i2 = end
        return 1
    
    # Removes the line which are not extendable from the extendable set 
    def CompleteExtension(self):
        
        rm = []
        end = self.new_count + self.window_size
        for t in self.extendresistance:
            if t in rm:
                continue
            c = self.CheckExtendLine(t, end, 1)
            if c == -1:
                rm.append(t)
                self.resistance.append(t)
        
        for t in rm:
            if t in self.extendresistance:
                self.extendresistance.remove(t)
                
        rm = []
        
        for t in self.extendsupport:
            if t in rm:
                continue
            c = self.CheckExtendLine(t, end, 0)
            if c == -1:
                rm.append(t)
                self.support.append(t)
        
        for t in rm:
            if t in self.extendsupport:
                self.extendsupport.remove(t)
        
            
    # roll window update function takes in the newData and verifies if the lines satisfies this point or not
    def RollWindowUpdate(self, newData):
        
        newData['minval'] = newData[['open', 'close']].min(axis = 1)
        newData['maxval'] = newData[['open', 'close']].max(axis = 1)
        
        self.new_count = self.new_count + 1
        index = self.new_count + self.window_size
        # self.df = pd.concat([self.df, newData.T])
        self.temp_df = pd.concat([self.temp_df, newData])
        
        if self.new_count > 0 : 
            if (self.new_count//self.n)*self.n == self.new_count :    
                self.UpdateExtremas()
            
            kwind = self.n*2
            if (self.new_count//kwind)*kwind == self.new_count:
                self.UpdateLines()    

        # New trend lines finding done
        
        xval = newData.iloc[0]['candleID']    
        yval = newData.iloc[0]['maxval']
        # Update resistance
        
        # First trying to find the previous resistance which are continuing
        rm = []
        
        for t in self.extendresistance:
            if t in rm:
                continue
            flag = 1
            c = self.ScoreLine(t, xval, yval, flag)
            if c == -1:
                rm.append(t)
                self.resistance.append(t)
            elif c == 0:
                t.score = t.score+1
                if t.score >= 10:
                    rm.append(t)
                    self.resistance.append(t)
            elif c == 1:
                t.i2 = xval
                if t.score < 10:
                    t.score = 1
                

        for t in rm:
            if t in self.extendresistance:
                self.extendresistance.remove(t) 
        
        # Extending the still valid support lines
        rm = []
        yval = newData.iloc[0]['minval']
        
        for t in self.extendsupport:
            if t in rm:
                continue
            flag = 0
            c = self.ScoreLine(t, xval, yval, flag)
            if c == -1:
                rm.append(t)
                self.support.append(t)
            elif c == 0:
                t.score = t.score+1
                if t.score >= 10:
                    rm.append(t)
                    self.support.append(t)
            elif c == 1:
                t.i2 = xval
                if t.score < 10:
                    t.score = 1

        for t in rm:
            if t in self.extendsupport:
                self.extendsupport.remove(t) 
                
                
    def plotTrends(self, fname, flag:int = 0):
        fig = PlotAddCandleSticks(self.df)
        c = 0
        for i in self.support:
            c = c+1
            if i.i2 < self.df.shape[0] - self.n*5:
                continue
            name = 'support_' + str(c)
            PlotAddTrendLine(fig, i, name)
        
        for i in self.resistance:
            c = c+1
            if i.i2 < self.df.shape[0] - self.n*5:
                continue
            name = 'resistance_' + str(c)
            PlotAddTrendLine(fig, i, name)
        
        
        for i in self.extendsupport:
            c = c+1
            if i.i2 < self.df.shape[0] - self.n*5:
                continue
            name = 'extend_support_' + str(c)
            PlotAddTrendLine(fig, i, name)
        
        for i in self.extendresistance:
            c = c+1
            if i.i2 < self.df.shape[0] - self.n*5:
                continue
            name = 'extend_resistance_' + str(c)
            PlotAddTrendLine(fig, i, name)
        
        plot(fig, fname + '.html')

# if __name__ == '__main__':
    
#     from copy import deepcopy
    
#     Data = TradingData('ETH', '4h', '12/1/2021 00:00:00', '2/1/2022 23:59:59')
#     n = 20
#     df = deepcopy(Data.df[:n*10+1])
#     trendObj = TrendLineAnalysis(df, n)
    
#     for i in range(Data.df.shape[0]):
#         if i <= n*10+1:
#             continue 
#         p3 = Data.df.iloc[i]    
#         p3 = pd.DataFrame(p3)
#         p3 = p3.T
        
#         diff = i - n*10
#         if (diff//(n*2))*(n*2) == diff:     
#             trendObj.plotTrends('ETH')
    
#         trendObj.RollWindowUpdate(p3)    
#         trendObj.FilterResistanceLines()
#         trendObj.FilterSupportLines()
    
    
#     trendObj.UpdateExtremas()
#     trendObj.UpdateLines()
#     trendObj.FilterResistanceLines()
#     trendObj.FilterSupportLines()
    
#     trendObj.plotTrends('ETH')